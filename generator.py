import json
import uuid
import os
import sys
import argparse

def find_view_to_update(parent):
    filelist = os.listdir(os.getcwd() + "\\views\\")
    path = os.getcwd() + "\\views\\"
    for x in filelist:
        print(path + x)
        with open(path + x,"r") as f:
            data = f.read()
            print(data)
            try:
                obj = json.loads(data)
                if obj["folderName"] == parent:
                    return obj["id"]
            except:
                print("unable to pull data")

def find_object_id(objectName):
    path = os.getcwd() + "\\objects\\" + objectName + "\\" + objectName + ".yy"
    with open(path,"r") as of:
        data = of.read()
        print(data)
        try:
            obj = json.loads(data)
            return obj["id"]
        except:
            print("object doesn't exist")


def generate_uuid():
    return str(uuid.uuid4())

def update_views_folder(key, folderName):
    #add folder to views section and generate view.yy file
    with open("generation_templates/obj_folder_template.yy","r") as folder_view_template:
        #load template and convert into dictionary
        json_string = folder_view_template.read()
        obj_folder_json = json.loads(json_string)

        print(json_string)
        print(key)

        #alter/update template with appropriate values
        obj_folder_json["id"] = key
        obj_folder_json["name"] = key
        obj_folder_json["folderName"] = folderName

        print("altered folder object")
        #dump dictionary as string and save as variable
        new_folder = json.dumps(obj_folder_json, indent=4, sort_keys=True)
        print(new_folder)

    #We write the new folder object to the views folder
    with open("views/"+ key + ".yy","w") as folder_view_file:
        folder_view_file.write(new_folder)

def update_parent_folder_view(key, parent = "objects"):
    #open the objects folder and add the key for our folder to its children
    parent_view_key = find_view_to_update(parent)
    with open("views/" + parent_view_key + ".yy", "r") as obj_resc_folder:
        orf_data = json.loads(obj_resc_folder.read())
        orf_data["children"].append(key)
        
        #writing data back to the file
        with open("views/" + parent_view_key + ".yy", "w") as orf_updated_file:
            orf_updated_file.write(json.dumps(orf_data, indent=4, sort_keys=True))   

#Object Bit
def update_objects_folder(key, objectName, parent = "none"):
    with open("generation_templates/object_template.yy","r") as object_view_template:
        #load template and convert into dictionary
        json_string = object_view_template.read()
        object_json = json.loads(json_string)
        
        #alter/update template with appropriate values
        object_json["id"] = key
        object_json["name"] = objectName
        #adding a step event to our object...
        with open("generation_templates/event_step_template.txt") as step_event_template:
            ev_json_string = step_event_template.read()
            ev_json_obj = json.loads(ev_json_string)

        with open("generation_templates/event_create_template.txt") as create_event_template:
            ev_cr_json_string = create_event_template.read()
            ev_cr_json_obj = json.loads(ev_cr_json_string)

        ev_json_obj["id"] = generate_uuid()
        ev_json_obj["m_owner"] = key

        ev_cr_json_obj["id"] = generate_uuid()
        ev_cr_json_obj["m_owner"] = key

        object_json["eventList"].append(ev_json_obj)
        object_json["eventList"].append(ev_cr_json_obj)
        if(parent != "none"):
            parentId = find_object_id(parent)
            object_json["parentObjectId"] = parentId

        #dump dictionary as string and save as variable
        new_object = json.dumps(object_json, indent=4, sort_keys=True)
        print(new_object)

    #We write the new object to the views folder
    #But first we need to create a directory in our objects folder to place our new object
    os.mkdir("objects/" + objectName)
    with open("objects\\"+ objectName + "\\" +  objectName + ".yy","w") as object_yy_file:
        object_yy_file.write(new_object)

    #Generate appropriate code for step event based upon whether the object is a state or behavior
    if(parent == "none"):
        with open("generation_templates/state_event_code.gml") as sec:
            event_code = sec.read()
    else:
        with open("generation_templates/behavior_event_code.gml") as bec:
            event_code = bec.read()

    #write step code to object folder   
    with open("objects\\"+ objectName + "\\" + "Step_0.gml","w") as object_step_file:
        object_step_file.write(event_code)
    #write create code to object folder
    with open("objects\\"+ objectName + "\\" + "Create_0.gml","w") as object_step_file:
        if("state" in objectName):
            enumName = objectName.replace('state_','')
            object_step_file.write("enum " + enumName + " {\n\t name, \n}")


def update_project_resources(key, id, resc_type, resc_name = "default"):
    #We open the project .yyp file

    dirpath = os.getcwd()
    foldername = os.path.basename(dirpath)
    print(foldername)
    with open(foldername + ".yyp","r") as pf:

        #read the file and convert contents into dictionary
        project_file = pf.read()
        project_json = json.loads(project_file)

        print(resc_type)
        if(resc_type == "folder"):
            #open the resource template for a folder
            with open("generation_templates/folder_resource_template.txt", "r") as frt:
                #convert template to dictionary
                new_resc = frt.read()
                new_resc_obj = json.loads(new_resc)
            
            #Add values and fill out template, generate separate resource id
            new_resc_obj["Key"] = key
            new_resc_obj["Value"]["id"] = id
            new_resc_obj["Value"]["resourcePath"] =  "views\\" + key + ".yy"
            print("new resource to be added")
            print(json.dumps(new_resc_obj, indent=4, sort_keys=True))
        if(resc_type == "object"):
            #open the resource template for an object
            with open("generation_templates/object_resource_template.txt", "r") as ort:
                #convert template to dictionary
                new_resc = ort.read()
                new_resc_obj = json.loads(new_resc)                
                
            #Add values and fill out template, generate separate resource id
            new_resc_obj["Key"] = key
            new_resc_obj["Value"]["id"] = id
            new_resc_obj["Value"]["resourcePath"] =  "objects\\" + resc_name + "\\" + resc_name + ".yy"


        #We append the resource object to the resources list in our project dictionary...
        print("appending the resource to the project file:")
        project_json["resources"].append(new_resc_obj)
        print(json.dumps(project_json, indent=4, sort_keys=True))
        updated_project_data = json.dumps(project_json, indent=4, sort_keys=True)
    #Finally we write to the project file overriding the current file...
    with open(foldername + ".yyp","w") as updated_project_file:
        updated_project_file.write(updated_project_data)

def create_folder_resource(name, parent = "objects"):
    resc_key = generate_uuid()
    resc_id  = generate_uuid()

    update_views_folder(resc_key,name)
    update_parent_folder_view(resc_key, parent)
    update_project_resources(resc_key,resc_id,"folder")

def create_object_resource(name, parent = "none", folder = "objects"):
    resc_key = generate_uuid()
    resc_id  = generate_uuid()

    update_objects_folder(resc_key,name,parent)
    update_parent_folder_view(resc_key, folder)
    update_project_resources(resc_key,resc_id,"object",name)   



def check_folders(name):
    filelist = os.listdir(os.getcwd() + "\\views\\")
    path = os.getcwd() + "\\views\\"
    for x in filelist:
        print(path + x)
        with open(path + x,"r") as f:
            data = f.read()
            print(data)
            try:
                obj = json.loads(data)
                if obj["folderName"] == name:
                    return True
            except:
                print("unable to pull data")    
    return False


parser = argparse.ArgumentParser(description='Create States & Behaviors')
parser.add_argument('-o', '--obj', type=str, metavar='', required=True, help='Object to attch to')
parser.add_argument('-n', '--name', type=str, metavar='', required=True, help='Name of resource')
parser.add_argument('-p', '--parent_folder', type=str, metavar='', help='Parent folder')
group = parser.add_mutually_exclusive_group()
group.add_argument('-s', '--create_state', action='store_true', help='Creates State in GMS2')
group.add_argument('-b', '--create_behavior', action='store_true', help='Creates Behavior in GMS2')
group.add_argument('-f', '--create_folder', action='store_true', help='Creates Folder in GMS2')

args = parser.parse_args()

try:
    if(args.create_folder):
        create_folder_resource(args.name,args.parent)
    elif(args.create_state):
        state_name = "state_" + args.name
        behavior_name = "bh_" + args.name + "_" + args.obj
        if(check_folders("states") and check_folders("behaviors")):
            create_object_resource(state_name,"none","states")
            bh_folder = args.name
            create_folder_resource(bh_folder,"behaviors")
            create_object_resource(behavior_name,state_name,bh_folder)
        else:
            bh_folder = args.name
            create_folder_resource("states")
            create_folder_resource("behaviors")
            create_folder_resource(bh_folder,"behaviors")
            create_object_resource(state_name,"none","states")
            create_object_resource(behavior_name,state_name,bh_folder)
    elif(args.create_behavior):
        bh_folder = args.name
        parent_state = "state_" + args.name
        behavior_name = "bh_" + args.name + "_" + args.obj

        if(check_folders(bh_folder)):
            create_object_resource(behavior_name,parent_state,bh_folder)
        else:
            print("unable to find behavior folder...")
            print("please make sure a state has been created with the name: ")
            print(parent_state)
    else:
        print("Please specify what you want to do. Press -h for details...")
except:
    print("Something went wrong :(")
    print("Maybe you already defined that resource?")