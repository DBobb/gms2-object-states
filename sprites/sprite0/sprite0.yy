{
    "id": "151c4ff1-8717-4fdc-a03a-713eb65c30a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93e47a9b-e184-4e06-9201-6a95f9d272c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "151c4ff1-8717-4fdc-a03a-713eb65c30a9",
            "compositeImage": {
                "id": "53632061-3a6c-44ef-9358-934e03708e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e47a9b-e184-4e06-9201-6a95f9d272c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2464490-1344-439e-a470-bc8945b31fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e47a9b-e184-4e06-9201-6a95f9d272c9",
                    "LayerId": "57f53dc4-5e7d-49f5-8a29-14a2efdbc702"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "57f53dc4-5e7d-49f5-8a29-14a2efdbc702",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "151c4ff1-8717-4fdc-a03a-713eb65c30a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}