
//Object Vars and Init
spd = 0;
show_debug_overlay(true);



//AI initialization scripts
use_state();
use_tracks();

//Creating a new track
horiz_track = create_track(trackType.normal);
vert_track = create_track(trackType.normal);

//Creating behaviors to add to tracks...
r = create_behavior(state_move_right,[move_right.spd,4]);
l = create_behavior(state_move_left,[move_left.spd,4]);
d = create_behavior(state_move_down,[move_down.spd,4]);
u = create_behavior(state_move_up,[move_up.spd,4]);
w = create_behavior(state_wait,[wait.time,3]);

if(!using_tracks)
	current_behavior = undefined; //initialize current behavior if not using tracks

ds_list_add(horiz_track[track.id],r,l,w);
ds_list_add(vert_track[track.id],u,d,w);
