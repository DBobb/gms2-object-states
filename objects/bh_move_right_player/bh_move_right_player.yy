{
    "id": "87c2ac87-72a0-492f-a855-5ae6b632787d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bh_move_right_player",
    "eventList": [
        {
            "id": "3353c7bd-4b3c-4cd2-9f80-683ec459b93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87c2ac87-72a0-492f-a855-5ae6b632787d"
        },
        {
            "id": "881cf304-5510-42e6-8f5b-e05722adb28f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87c2ac87-72a0-492f-a855-5ae6b632787d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "acea2f34-23d0-4a33-8101-418c0d3c925b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}