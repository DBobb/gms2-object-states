/// @description Behavior object
// A behavior instance is derived from a state. It inherits step code,
// and contains the entry and exit conditions specific to the instance.

// Initialize State
if(state_status == state.enter_state) {
	state_status = state.running_state;
	
}

// Exit conditions go here
if(y >= room_height - sprite_height) {
	state_status = state.exit_state;
}
// Inherit the parent event
if(state_status == state.running_state)
	event_inherited();

// Run on exit
if(state_status == state.exit_state && !using_tracks) {
	state_status = state.enter_state;

}