{
    "id": "33b108a9-6ff7-4898-bed0-b4ce755923b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bh_move_down_player",
    "eventList": [
        {
            "id": "d5b45c10-cddc-4d31-b173-1c40b2a6ffb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33b108a9-6ff7-4898-bed0-b4ce755923b4"
        },
        {
            "id": "035a3bc6-7097-4e49-98a9-b8d6a99abdb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33b108a9-6ff7-4898-bed0-b4ce755923b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fd8b1bae-2ee7-4a5b-ac7b-7e6e2a3a21ed",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}