{
    "eventList": [
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "id": "a7e2ab5e-dd62-4e63-b802-13f0acf63ece",
            "m_owner": "803f60ad-bb12-4ecd-8120-70d0bab041a0",
            "modelName": "GMEvent",
            "mvc": "1.0"
        },
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "id": "085fea12-0dd8-4eef-93df-7923833fe2ef",
            "m_owner": "803f60ad-bb12-4ecd-8120-70d0bab041a0",
            "modelName": "GMEvent",
            "mvc": "1.0"
        }
    ],
    "id": "803f60ad-bb12-4ecd-8120-70d0bab041a0",
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "state_try",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}