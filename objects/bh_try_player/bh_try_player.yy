{
    "eventList": [
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "id": "cf124e20-483a-423e-9078-62b8193a6f32",
            "m_owner": "b701f977-0fa8-428f-844b-156808f35106",
            "modelName": "GMEvent",
            "mvc": "1.0"
        },
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "id": "b888a253-b720-45d2-bf35-310f78272fc1",
            "m_owner": "b701f977-0fa8-428f-844b-156808f35106",
            "modelName": "GMEvent",
            "mvc": "1.0"
        }
    ],
    "id": "b701f977-0fa8-428f-844b-156808f35106",
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bh_try_player",
    "overriddenProperties": null,
    "parentObjectId": "803f60ad-bb12-4ecd-8120-70d0bab041a0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}