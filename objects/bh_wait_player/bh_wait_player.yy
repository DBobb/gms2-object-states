{
    "id": "ebf70c6f-884f-4627-a872-c3190b9f64f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bh_wait_player",
    "eventList": [
        {
            "id": "9cec936a-b13f-47b1-bed5-ef518b048aed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebf70c6f-884f-4627-a872-c3190b9f64f8"
        },
        {
            "id": "0c68fac0-312e-4522-9fb2-95bb0fe5b570",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ebf70c6f-884f-4627-a872-c3190b9f64f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d29aefa8-0828-4555-b2a9-b06b2db18455",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}