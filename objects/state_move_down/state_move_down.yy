{
    "id": "fd8b1bae-2ee7-4a5b-ac7b-7e6e2a3a21ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "state_move_down",
    "eventList": [
        {
            "id": "1e408f4b-9da4-4422-96ba-d0ae495ec3ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd8b1bae-2ee7-4a5b-ac7b-7e6e2a3a21ed"
        },
        {
            "id": "03c9f717-bd85-4b03-a5a6-e4ee0791ed22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd8b1bae-2ee7-4a5b-ac7b-7e6e2a3a21ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}