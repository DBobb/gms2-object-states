{
    "eventList": [
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "id": "4a36b840-6b51-4307-a046-6b06151766f4",
            "m_owner": "f1e4dd56-b174-4d17-91c6-a5f6ff4a9f18",
            "modelName": "GMEvent",
            "mvc": "1.0"
        },
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "id": "d96aee2f-b7b0-498a-9bac-a0172e185eed",
            "m_owner": "f1e4dd56-b174-4d17-91c6-a5f6ff4a9f18",
            "modelName": "GMEvent",
            "mvc": "1.0"
        }
    ],
    "id": "f1e4dd56-b174-4d17-91c6-a5f6ff4a9f18",
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bh_demo_player",
    "overriddenProperties": null,
    "parentObjectId": "823f2caa-a9a4-46a8-a6ec-9143e26637b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}