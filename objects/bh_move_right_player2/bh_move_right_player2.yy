{
    "id": "650c555b-fd4a-48c6-adba-d4b5324e0e3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bh_move_right_player2",
    "eventList": [
        {
            "id": "7a588f67-d716-4f9e-8141-1953ca46ecb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "650c555b-fd4a-48c6-adba-d4b5324e0e3b"
        },
        {
            "id": "79222709-a67d-4a9f-9ef1-82f6d8e20316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "650c555b-fd4a-48c6-adba-d4b5324e0e3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "acea2f34-23d0-4a33-8101-418c0d3c925b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}