/// @description Insert description here
// You can write your code in this editor

//Initialize State
if(state_status == state.enter_state) {
	state_status = state.running_state;
	spd = current_behavior[move_right.spd]; 
	timer_reset();
}

//Exit Conditions
if(x >= room_width - sprite_width) {
	state_status = state.exit_state;
}

// Run state
if(state_status == state.running_state) {
	event_inherited();
	timer_inc();
}

// Run exit code as necessary
if(state_status == state.exit_state && !using_tracks) {
	state_status = state.enter_state;
	current_behavior = create_behavior(state_move_left,[move_left.spd,30]);
}