{
    "eventList": [
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "id": "02643c52-2ed0-4c6d-9f13-45ad8fdb5ea4",
            "m_owner": "823f2caa-a9a4-46a8-a6ec-9143e26637b2",
            "modelName": "GMEvent",
            "mvc": "1.0"
        },
        {
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "id": "30800d8f-ba1a-4e33-ae5a-1345cb473ee9",
            "m_owner": "823f2caa-a9a4-46a8-a6ec-9143e26637b2",
            "modelName": "GMEvent",
            "mvc": "1.0"
        }
    ],
    "id": "823f2caa-a9a4-46a8-a6ec-9143e26637b2",
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "state_demo",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}