/// @description Insert description here
// You can write your code in this editor

//Initialize State
if(state_status == state.enter_state) {
	state_status = state.running_state;
	spd = current_behavior[move_up.spd]
	timer_reset();
}

//Exit Conditions
if(y <= 0) {
	state_status = state.exit_state;
}

/*if(timer() >= secs(1)) {
	state_status = state.exit_state;
}*/

// Inherit the parent event
if(state_status == state.running_state) {
	event_inherited();
	timer_inc();
}
// Run exit code if necessary
if(state_status == state.exit_state && !using_tracks) {
	state_status = state.enter_state;
}