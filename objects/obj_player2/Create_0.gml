/// @description Insert description here
// You can write your code in this editor

spd = 1;
use_tracks();
state_status = state.enter_state;

horiz_track = create_track(trackType.normal);
vert_track = create_track(trackType.normal);

r = create_behavior(state_move_right,[move_right.spd,53]);
l = create_behavior(state_move_left,[move_left.spd,50]);
d = create_behavior(state_move_down,[move_down.spd,4]);
u = create_behavior(state_move_up,[move_up.spd,5]);

ds_list_add(horiz_track[track.id],r,l);
ds_list_add(vert_track[track.id],u,d);