{
    "id": "25cb8032-831f-437b-961c-da6195d160b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2",
    "eventList": [
        {
            "id": "8e1c96d4-95ed-4c4e-8989-15f234c06a0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25cb8032-831f-437b-961c-da6195d160b5"
        },
        {
            "id": "f4d17c75-5a97-4ab1-89b9-633f84eb9dbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "25cb8032-831f-437b-961c-da6195d160b5"
        },
        {
            "id": "98b23880-7642-493f-8058-bb21762f8f85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "25cb8032-831f-437b-961c-da6195d160b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "835f814d-8d1c-444e-9459-48c6c6078a87",
    "visible": true
}