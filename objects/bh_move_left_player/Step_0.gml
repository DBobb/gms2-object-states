/// @description Insert description here
// You can write your code in this editor

//Initialize State
if(state_status == state.enter_state) {
	state_status = state.running_state;
	sprite_index = sprite0;
	spd = current_behavior[move_left.spd]
}

//Exit Conditions
if(x <= 0) {
	state_status = state.exit_state;
	if(!using_tracks) {}
}

// Inherit the parent event
if(state_status == state.running_state)
	event_inherited();

// Run exit code as necessary
if(state_status == state.exit_state && !using_tracks) {

		state_status = state.enter_state;
		current_behavior = create_behavior(state_move_right,[move_right.spd,7]);
	
}