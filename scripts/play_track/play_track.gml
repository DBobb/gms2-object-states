var this_track = argument0;

if(this_track[track.enabled]) {
	var behavior = ds_list_find_value(this_track[track.id],this_track[track.pos]);
	current_behavior = behavior;
	state_status = this_track[track.status]
	
	this_track[@ track.status] = state_machine(this_track[track.id]);
	
	switch(this_track[track.type]) {
		case trackType.normal: {
			if(state_status == state.exit_state) {
				//Advance to next behavior in track once complete
				if(this_track[track.pos] == ds_list_size(this_track[track.id]) - 1){
					this_track[@ track.pos] = 0;
					this_track[@ track.status] = state.enter_state;
					current_behavior = this_track[track.pos];
				} else {
					this_track[@ track.pos] = this_track[track.pos] + 1;
					this_track[@ track.status] = state.enter_state;
					current_behavior = this_track[track.pos];
				}
			}			
		}
		break;
		case trackType.reverse: {
			if(state_status == state.exit_state) {
				//Advance to next behavior in track once complete
				if(this_track[track.pos] == 0){
					this_track[@ track.pos] = ds_list_size(this_track[track.id]) - 1;
					this_track[@ track.status] = state.enter_state;
					current_behavior = this_track[track.pos];
				} else {
					this_track[@ track.pos] = this_track[track.pos] - 1;
					this_track[@ track.status] = state.enter_state;
					current_behavior = this_track[track.pos];
				}
			}			
		}
		break;
		case trackType.rand: {
			if(state_status == state.exit_state) {
				this_track[@ track.pos] = irandom_range(0,(ds_list_size(this_track[track.id])-1))
				this_track[@ track.status] = state.enter_state;
				current_behavior = this_track[track.pos];
			}			
		}
		break;
	}

}

