enum track {
	id,
	type,
	enabled,
	pos,
	status,
}

enum trackType {
	normal,
	reverse,
	rand,
}

enum state {
	enter_state,
	exit_state,
	running_state,
}