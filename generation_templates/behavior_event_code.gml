/// @description Behavior object
// A behavior instance is derived from a state. It inherits step code,
// and contains the entry and exit conditions specific to the instance.

// Initialize State
if(state_status == state.enter_state) {
	state_status = state.running_state;
	timer_reset();

}

// Exit conditions go here

// Inherit the parent event
if(state_status == state.running_state) {
	event_inherited();
	timer_inc();
}
// Run on exit
if(state_status == state.exit_state) {

	if(!using_tracks) {
		state_status = state.enter_state;
	}
}